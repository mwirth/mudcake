using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class Tweening : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Tween.LocalScale(transform, new Vector3(1.2f, 1.2f, 1.2f), 0.3f, delay: 0, Tween.EaseInOutStrong);
        Tween.LocalScale(transform, new Vector3(1f, 1f, 1f), 0.5f, delay: 0.3f, Tween.EaseInOutStrong);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
