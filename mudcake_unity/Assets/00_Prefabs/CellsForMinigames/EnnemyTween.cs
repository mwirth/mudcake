using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class EnnemyTween : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Tween.LocalPosition(transform, new Vector3(0.05f,0,0), 0.1f, delay: 0, Tween.EaseSpring, Tween.LoopType.PingPong);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
