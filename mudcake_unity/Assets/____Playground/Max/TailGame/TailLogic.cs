using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;

public class TailLogic : MonoBehaviour
{
    public int deadPlayers;
    private MiniGameLogic minigameLogic;
    public int activeTentacles = 0;
    private WorldSH sound;

    private void Start()
    {
        minigameLogic = GetComponentInParent<MiniGameLogic>();
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        sound.TentacleMoving(transform, activeTentacles);
        activeTentacles++;
    }

    private void Update()
    {
        if (deadPlayers == NetworkManager.Singleton.ConnectedClients.Count && NetworkManager.Singleton.ConnectedClients.Count != 0) minigameLogic.LoseMinigame();
        else if (deadPlayers == 4) minigameLogic.LoseMinigame();
    }

    private void OnDisable()
    {
        for (int i = 0; i < activeTentacles; i++)
        {
            sound.StopTentacleSound(i);
        }

        activeTentacles = 0;
    }
}
