using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class scr_BodyRotation : MonoBehaviour
{
    public Transform pivot;
    public float maxRotationSpeed = 50f;
    public float maxFrequency = 3f;
    float timeLeft, rotZ;

    public GameObject Body;
    float RandomRot;
    float RandomScale;
    Vector3 BodyPos;
    public GameObject Tail;
    public float MaxScale;
    public float MinScale;
 

    float XPos;
    float YPos;
    int NumberPos;

    Mudcake_actions myControls;

    public float respawn;
    public List<GameObject> TailHeads = new List<GameObject>();

    private WorldSH sound;

    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        myControls = new Mudcake_actions();
        myControls.Minigames.Qkey.performed += ctx => q();
    }


    void Start()
    {
        TailHeads.AddRange(GameObject.FindGameObjectsWithTag("TailHead"));
    }

    // Update is called once per frame
    void Update()
    {
        //Rotation
        timeLeft -= Time.deltaTime;
        if(timeLeft <= 0)
        {
            rotZ = Random.Range(-maxRotationSpeed, maxRotationSpeed);
            timeLeft = Random.Range(0, maxFrequency);
        }

        pivot.Rotate(pivot.rotation.x, rotZ * Random.Range(0.1f, 1f) * Time.deltaTime, pivot.rotation.x, Space.Self);

    }

    public void SpawnObj()
    {

        BodyPos = transform.position;
        NumberPos = Random.Range(1, 5);
        RandomRot = Random.Range(0, 360);
        RandomScale = Random.Range(MinScale, MaxScale);

        if (NumberPos == 1)
        {
            SpawnTail();
        }

        if (NumberPos == 2)
        {
            SpawnTail();
        }

        if (NumberPos == 3)
        {
            SpawnTail();

        }

        if (NumberPos == 4)
        {
            SpawnTail();

        }

    }

    void SpawnTail()
    {
        GameObject newTailObject = Instantiate(Tail, new Vector3(BodyPos.x, BodyPos.y, BodyPos.z), transform.rotation * Quaternion.Euler(0, RandomRot,0));
        GameObject newHeadObject = newTailObject.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).GetChild(0).gameObject;
        TailHeads.Add(newHeadObject);
        newTailObject.transform.parent = Body.transform;
        newTailObject.transform.localScale = new Vector3(RandomScale, RandomScale, RandomScale);
        sound.TentacleGrowing(newTailObject.transform);
    }

    public void SpawnTailHead()
    {
        StartCoroutine(Delay());
    }


    IEnumerator Delay()
    {
        yield return new WaitForSeconds(respawn);
        foreach (GameObject Head in TailHeads)
        {
            Debug.Log("entered");
            Head.SetActive(true);
        }

    }

    //here are input system stuff again-->
    private void q()
    {
        SpawnObj();
    }

    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }

}
