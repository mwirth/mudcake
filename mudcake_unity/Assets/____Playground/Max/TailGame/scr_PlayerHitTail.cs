using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PlayerHitTail : MonoBehaviour
{
    public GameObject Body;
    private scr_BodyRotation script;
    public float healthValue = 100;
    private float maxHealth;
    public GameObject healthBar;
    public float decaySpeed = 1;
    private TailLogic tailLogic;
    private WorldSH sound;
    private Transform centerTentacle;

    private void Start()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        maxHealth = healthValue;
        tailLogic = GetComponentInParent<TailLogic>();
        centerTentacle = tailLogic.GetComponent<Transform>();
        script = Body.GetComponent<scr_BodyRotation>();
        //TailHeads.Add(objektname)
    }

    private void Update()
    {
        healthValue -= Time.deltaTime * decaySpeed;
        healthBar.transform.localScale = new Vector3(healthValue/100, 1, 1);
        if (healthValue < 0)
        {
            //lose consequences
            tailLogic.deadPlayers++;
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "TailHead")
        {
            sound.CollectTentacleBlop(other.transform);
            sound.TentacleMoving(centerTentacle, tailLogic.activeTentacles);
            tailLogic.activeTentacles++;
            healthValue = maxHealth;
            script.SpawnObj();
            other.gameObject.SetActive(false);
            script.SpawnTailHead();
        }

        if (other.gameObject.tag == "Tail")
        {
            tailLogic.deadPlayers++;
            gameObject.SetActive(false);
            //Destroy(gameObject);

        }


    }
}
