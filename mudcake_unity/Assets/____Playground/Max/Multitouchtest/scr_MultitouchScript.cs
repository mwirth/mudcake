using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;

public class scr_MultitouchScript : MonoBehaviour
{
    Mudcake_actions myControls;
    private Vector2 mousePosition;
    private Vector2 mousePosition1;
    private Vector2 mousePosition2;
    private Vector2 mousePosition3;

    private Vector3 playerPosition;
    private Vector3 playerobjPosition1;
    private Vector3 playerobjPosition2;
    private Vector3 playerobjPosition3;
    public GameObject Player1;
    public GameObject Player2;
    public GameObject Player3;
    public GameObject Player4;
    private Camera mainCamera;

    private RaycastHit hitClick;



    //DragCrown
    bool dragged;
    bool dragged1;
    bool dragged2;
    bool dragged3;
    public float moveSpeed = 0.1f;
    private Plane daggingPlane;
    private Plane daggingPlane1;
    private Plane daggingPlane2;
    private Plane daggingPlane3;
    private Vector3 offset;

    private void Awake()
    {
        myControls = new Mudcake_actions();
        myControls.Minigames.MousePosition.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
        myControls.Minigames.MousePosition1.performed += ctx => mousePosition1 = ctx.ReadValue<Vector2>();
        myControls.Minigames.MousePosition2.performed += ctx => mousePosition2 = ctx.ReadValue<Vector2>();
        myControls.Minigames.MousePosition3.performed += ctx => mousePosition3 = ctx.ReadValue<Vector2>();
    }

    void Update()
    {
        var myRay = mainCamera.ScreenPointToRay((mousePosition));
        Physics.Raycast(myRay, out hitClick);

        Debug.Log(mousePosition);

        if (dragged == true)
        {
            //Vector3 mousePositionNew = new Vector3(mousePosition.x, 0, mousePosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            float planeDistance;
            daggingPlane.Raycast(camRay, out planeDistance);
            transform.position = camRay.GetPoint(planeDistance) + offset;

        }

        if (dragged1 == true)
        {
            //Vector3 mousePositionNew = new Vector3(mousePosition.x, 0, mousePosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            float planeDistance;
            daggingPlane1.Raycast(camRay, out planeDistance);
            transform.position = camRay.GetPoint(planeDistance) + offset;

        }

        if (dragged2 == true)
        {
            //Vector3 mousePositionNew = new Vector3(mousePosition.x, 0, mousePosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            float planeDistance;
            daggingPlane2.Raycast(camRay, out planeDistance);
            transform.position = camRay.GetPoint(planeDistance) + offset;

        }

        if (dragged3 == true)
        {
            //Vector3 mousePositionNew = new Vector3(mousePosition.x, 0, mousePosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            float planeDistance;
            daggingPlane3.Raycast(camRay, out planeDistance);
            transform.position = camRay.GetPoint(planeDistance) + offset;

        }
    }

    private void OnMyClick()
    {

        //if (hitClick.collider.CompareTag("Enemy"))
        //{
        //Destroy(hitClick.collider.gameObject);
        //}

        if (hitClick.collider.CompareTag("Player1"))
        {
            dragged = true;
            daggingPlane = new Plane(mainCamera.transform.forward,
                      transform.position);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            daggingPlane.Raycast(camRay, out planeDistance);
            offset = transform.position - camRay.GetPoint(planeDistance);

        }
        if (hitClick.collider.CompareTag("Player2"))
        {
            dragged1 = true;
            daggingPlane1= new Plane(mainCamera.transform.forward,
                      transform.position);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition1);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            daggingPlane1.Raycast(camRay, out planeDistance);
            offset = transform.position - camRay.GetPoint(planeDistance);

        }
        if (hitClick.collider.CompareTag("Player3"))
        {
            dragged2 = true;
            daggingPlane2 = new Plane(mainCamera.transform.forward,
                      transform.position);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition2);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            daggingPlane2.Raycast(camRay, out planeDistance);
            offset = transform.position - camRay.GetPoint(planeDistance);

        }
        if (hitClick.collider.CompareTag("Player4"))
        {
            dragged3 = true;
            daggingPlane3 = new Plane(mainCamera.transform.forward,
                      transform.position);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition3);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            daggingPlane3.Raycast(camRay, out planeDistance);
            offset = transform.position - camRay.GetPoint(planeDistance);

        }


    }

    private void OnMyRelease()
    {
        dragged = false;
        dragged1 = false;
        dragged2 = false;
        dragged3 = false;

    }


    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }

}
