using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Spieler : MonoBehaviour
{

    public scr_MovementJoystick movementJoystick;
    public float playerSpeed;
    private Rigidbody rb;
    public float scaler;
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Debug.Log(movementJoystick.joystickVec.y);
        if (movementJoystick.joystickVec.y != 0)
        {
            rb.velocity = new Vector3(movementJoystick.joystickVec.x * playerSpeed,0, movementJoystick.joystickVec.y * playerSpeed);
        }
        else
        {
            rb.velocity = Vector3.zero;
            rb.Sleep();
        }
    }

    void OnCollisionEnter(Collision targetObj)
    {
        if (targetObj.gameObject.tag == "Good")
        {
            Destroy(targetObj.gameObject);
            player.transform.localScale = new Vector3(player.transform.localScale.x + scaler, player.transform.localScale.y + scaler, player.transform.localScale.z + scaler);
        }

        if (targetObj.gameObject.tag == "Bad")
        {
            Destroy(targetObj.gameObject);
            player.transform.localScale = new Vector3(player.transform.localScale.x - scaler, player.transform.localScale.y - scaler, player.transform.localScale.z - scaler);

        }
    }
}
