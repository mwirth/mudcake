using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_PlayerCollect : MonoBehaviour
{
    public float scaler;
    public bool punish = true;
    public float punishTime;
    private CollectLogic collectLogic;
    private WorldSH sound;

    private void Start()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        collectLogic = GetComponentInParent<CollectLogic>();
    }

    private void Update()
    {
        if (transform.localScale.x < 0)
        {
            collectLogic.deadPlayers++;
            gameObject.SetActive(false);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Wall" && punish == true)
        {
            Debug.Log("entered");
            transform.localScale = new Vector3(transform.localScale.x - scaler, transform.localScale.y - scaler, transform.localScale.z - scaler);
            punish = false;
            StartCoroutine(Punishement());
        }

    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.tag == "Wall" && punish == true)
        {
            Debug.Log("entered");
            transform.localScale = new Vector3(transform.localScale.x - scaler, transform.localScale.y - scaler, transform.localScale.z - scaler);
            punish = false;
            StartCoroutine(Punishement());
        }
    }

    private void OnParticleCollision(GameObject other)
    {
        if (other.gameObject.tag == "Good")
        {
            sound.CollectGood(other.transform);
            transform.localScale = new Vector3(transform.localScale.x + scaler, transform.localScale.y + scaler, transform.localScale.z + scaler);
        }

        if (other.gameObject.tag == "Bad")
        {
            sound.CollectBad(other.transform);
            transform.localScale = new Vector3(transform.localScale.x - scaler, transform.localScale.y - scaler, transform.localScale.z - scaler);

        }
    }

    IEnumerator Punishement()
    {
        yield return new WaitForSeconds(punishTime);
        punish = true;
    }
}
