using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ObjectSpawn : MonoBehaviour
{

    public GameObject CollectablesGood;
    public GameObject CollectablesBad;
    public float spawnRateGood= 0.5f;
    private bool spawnGood;
    public float spawnRateBad = 0.5f;
    private bool spawnBad;

    private void Awake()
    {
        spawnGood= false;
        spawnBad = false;
    }


    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (spawnGood == false)
        {
            spawnGood = true;
            StartCoroutine(DelayGood());

        }

        if (spawnBad == false)
        {
            spawnBad = true;
            StartCoroutine(DelayBad());

        }
    }

    IEnumerator DelayGood()
    {
        yield return new WaitForSeconds(spawnRateGood);
            Instantiate(CollectablesGood, new Vector3(UnityEngine.Random.Range(-13f, 13f), 0f, 12f), Quaternion.identity, transform);

            spawnGood = false;
    }

    IEnumerator DelayBad()
    {
        yield return new WaitForSeconds(spawnRateBad);
        Instantiate(CollectablesBad, new Vector3(UnityEngine.Random.Range(-13f, 13f), 0f, 12f), Quaternion.identity, transform);

        spawnBad = false;
    }
}
