using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;

public class CollectLogic : MonoBehaviour
{
    public int deadPlayers;
    private MiniGameLogic minigameLogic;

    private void Start()
    {
        minigameLogic = GetComponentInParent<MiniGameLogic>();
    }

    private void Update()
    {
        if (deadPlayers == NetworkManager.Singleton.ConnectedClients.Count && NetworkManager.Singleton.ConnectedClients.Count != 0) minigameLogic.LoseMinigame();
        else if (deadPlayers == 4) minigameLogic.LoseMinigame();
    }
}
