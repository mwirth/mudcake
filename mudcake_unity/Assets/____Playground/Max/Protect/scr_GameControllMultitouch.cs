using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.EnhancedTouch;

public class scr_GameControllMultitouch : MonoBehaviour
{
    class FingerData
    {
        public Vector2 ScreenPosition;
        public bool Dragging;
        public Transform DraggedTransform;
        public Plane DraggingPlane;
    }


    //private Vector2 mousePosition;
    private Vector3 objPosition;
    private Camera mainCamera;

    private RaycastHit hitClick;
    //private RaycastHit hitClickEnemy;


    //DragCrown
    bool dragged;
    public float moveSpeed = 0.1f;
    private Plane daggingPlane;
    private Vector3 offset;
    private Transform draggedTransform;

    //Imputsystem

    Mudcake_actions myControls;
    private int playerID;
    private Vector2 touchPosition1;
    private Vector2 touchPosition2;
    private Vector2 touchPosition3;
    private Vector2 touchPosition4;

    private Vector2 FingerPosition;

    //fingers
    private List<FingerData> allFingers = new List<FingerData>();


    private int fingerP1;
    private int fingerP2;
    private int fingerP3;
    private int fingerP4;

    private int Fingerindex;

    private int currentTouchID;


    //Spawning
    public GameObject Enemy;
    public float spawnRate = 0.5f;
    private int spawnSide;
    private bool spawn;
    public List<GameObject> allEnemies = new List<GameObject>();

    //Sound
    public WorldSH sound;
    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());

        dragged = false;
        mainCamera = Camera.main;
        myControls = new Mudcake_actions();
        EnhancedTouchSupport.Enable();
    }

    private void Start()
    {
        spawnSide = UnityEngine.Random.Range(0, 4);

    }

    private void Update()
    {
        if (spawn == false)
        {
            spawn = true;
            StartCoroutine(Delay());

        }

    }

    private void Touch_onFingerUp(Finger finger)
    {
        OnMyRelease(finger.index);
    }

    private void Touch_onFingerMove(Finger finger)
    {
        OnTouchMove(finger.index);
    }

    private void Touch_onFingerDown(Finger finger)
    {
        Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count);
        OnMyClick(finger.index);
    }

    private void OnMyClick(int index)
    {
        if (index > 3)
            return;

        FingerData data = allFingers[index];

        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
        Selection(data);
    }

    private void OnTouchMove(int index)
    {
        if (index > 3)
            return;

        FingerData data = allFingers[index];

        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
        Draggin(data);
    }

    private void Selection(FingerData data)
    {

        var myRay = mainCamera.ScreenPointToRay(data.ScreenPosition);
        Physics.Raycast(myRay, out hitClick);

        if (hitClick.collider.CompareTag("Crown"))
        {
            data.DraggedTransform = hitClick.transform;
            data.Dragging = true;
            data.DraggingPlane = new Plane(mainCamera.transform.forward, data.DraggedTransform.position);
            Ray camRay = mainCamera.ScreenPointToRay(data.ScreenPosition);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            data.DraggingPlane.Raycast(camRay, out planeDistance);
            offset = data.DraggedTransform.position - camRay.GetPoint(planeDistance);

        }

        if (hitClick.collider.CompareTag("Enemy"))
        {
            Destroy(hitClick.collider.gameObject);
            sound.BlopEnemies(hitClick.collider.transform);
        }

    }

    private void Draggin(FingerData data)
    {
        if (data.Dragging == true)
        {
            Vector3 mousePositionNew = new Vector3(data.ScreenPosition.x, 0, data.ScreenPosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(data.ScreenPosition);
            float planeDistance;
            data.DraggingPlane.Raycast(camRay, out planeDistance);
            data.DraggedTransform.position = camRay.GetPoint(planeDistance) + offset;

        }
    }
    private void OnMyRelease(int index)
    {

        FingerData data = allFingers[index];
        data.Dragging = false;
    }


    private void OnEnable()
    {
        myControls.Minigames.Enable();
        TouchSimulation.Enable();

        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove += Touch_onFingerMove;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += Touch_onFingerUp;
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
        TouchSimulation.Disable();


        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove -= Touch_onFingerMove;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= Touch_onFingerUp;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //Whateverhappens
            Destroy(other.gameObject);

        }
    }



    IEnumerator Delay()
    {
        yield return new WaitForSeconds(spawnRate);

        if (spawnSide == 0)
        {
            GameObject newEnemy = Instantiate(Enemy, new Vector3(UnityEngine.Random.Range(-10f, 10f), 0f, 9f), Quaternion.identity, transform);
            allEnemies.Add(newEnemy);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 1)
        {
            GameObject newEnemy = Instantiate(Enemy, new Vector3(8, 0, UnityEngine.Random.Range(-6f, 6f)), Quaternion.identity, transform);
            allEnemies.Add(newEnemy);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 2)
        {
            GameObject newEnemy = Instantiate(Enemy, new Vector3(UnityEngine.Random.Range(-10f, 10f), 0, -9f), Quaternion.identity, transform);
            allEnemies.Add(newEnemy);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 3)
        {
            GameObject newEnemy = Instantiate(Enemy, new Vector3(-8, 0, UnityEngine.Random.Range(-6f, 6f)), Quaternion.identity, transform);
            allEnemies.Add(newEnemy);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
    }
}