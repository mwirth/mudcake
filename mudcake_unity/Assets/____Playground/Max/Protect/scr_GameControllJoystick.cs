using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;

public class scr_GameControllJoystick : MonoBehaviour
{
    Mudcake_actions myControls;
    private Vector2 mousePosition;
    private Vector3 objPosition;
    public GameObject Crown;
    public Rigidbody Crown_rdb;
    private Camera mainCamera;

    private RaycastHit hitClick;


    //Spawning
    public GameObject Enemy;
    float spawnRate = 0.5f;
    private int spawnSide;
    private bool spawn;


    //DragCrown
    bool dragged;
    public float moveSpeed = 0.1f;
    private Plane daggingPlane;
    private Vector3 offset;




    private void Awake()
    {
        dragged = false;
        spawn = false;
        mainCamera = Camera.main;
        myControls = new Mudcake_actions();
        myControls.Minigames.MousePosition.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
        myControls.Minigames.Click.performed += ctx => OnMyClick();
        myControls.Minigames.Release.performed += ctx => OnMyRelease();
    }

    private void Start()
    {
        spawnSide = UnityEngine.Random.Range(0, 4);

    }
    void Update()
    {
        var myRay = mainCamera.ScreenPointToRay((mousePosition));
        Physics.Raycast(myRay, out hitClick);
        if (spawn == false)
        {
            spawn = true;
            StartCoroutine(Delay());

        }

        if (dragged == true)
        {
            Vector3 mousePositionNew = new Vector3(mousePosition.x, 0, mousePosition.y);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            float planeDistance;
            daggingPlane.Raycast(camRay, out planeDistance);
            transform.position = camRay.GetPoint(planeDistance) + offset;

        }

    }

    void OnCollisionEnter(Collision targetObj)
    {
        if (targetObj.gameObject.tag == "Wall")
        {
            dragged = false;
        }
    }




    private void OnMyClick()
    {


        if (hitClick.collider.CompareTag("Rope"))
        {
            dragged = true;
            daggingPlane = new Plane(mainCamera.transform.forward,
                      transform.position);
            Ray camRay = mainCamera.ScreenPointToRay(mousePosition);
            Debug.DrawRay(camRay.origin, camRay.direction * 10, Color.green);

            float planeDistance;
            daggingPlane.Raycast(camRay, out planeDistance);
            offset = transform.position - camRay.GetPoint(planeDistance);

        }

    }

    private void OnMyRelease()
    {
        dragged = false;
        Crown_rdb.velocity = new Vector3(0, 0, 0);

    }


    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(spawnRate);

        if (spawnSide == 0)
        {
            Instantiate(Enemy, new Vector3(UnityEngine.Random.Range(-10f, 10f), 0f, 9f), Quaternion.identity);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 1)
        {
            Instantiate(Enemy, new Vector3(8, 0, UnityEngine.Random.Range(-6f, 6f)), Quaternion.identity);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 2)
        {
            Instantiate(Enemy, new Vector3(UnityEngine.Random.Range(-10f, 10f), 0, -9f), Quaternion.identity);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
        else if (spawnSide == 3)
        {
            Instantiate(Enemy, new Vector3(-8, 0, UnityEngine.Random.Range(-6f, 6f)), Quaternion.identity);
            spawnSide = UnityEngine.Random.Range(0, 4);
            spawn = false;
        }
    }
}