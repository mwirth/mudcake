using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;

public class CrownLogic : MonoBehaviour
{
    public float punishmentWhenHit = 5;
    public float survivalValueCrown = 100;
    public GameObject survivalBar;
    private MiniGameLogic minigameLogic;

    private void Start()
    {
        minigameLogic = transform.GetComponentInParent<MiniGameLogic>();
    }

    void Update()
    {
        if (survivalValueCrown < 0)
        {
            //lose consequences here
            minigameLogic.LoseMinigame();
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Enemy"))
        {
            //Whateverhappens
            Destroy(other.gameObject);
            survivalValueCrown -= punishmentWhenHit;
            Vector3 barScale = survivalBar.transform.localScale;
            survivalBar.transform.localScale = new Vector3(survivalValueCrown / 100, barScale.y, barScale.z);
        }
    }
}
