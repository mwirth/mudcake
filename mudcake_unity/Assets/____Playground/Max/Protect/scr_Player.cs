using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_Player : MonoBehaviour
{
    public scr_MovementJoystick movementJoystick;
    public float playerSpeed;
    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Debug.Log(movementJoystick.joystickVec.y);
        if(movementJoystick.joystickVec.y != 0)
        {
            rb.velocity = new Vector3(movementJoystick.joystickVec.x * playerSpeed,0, movementJoystick.joystickVec.y * playerSpeed);
        }
        else
        {
            rb.velocity = Vector3.zero;
            rb.Sleep();
        }
    }

    void OnCollisionEnter(Collision targetObj)
    {
        if (targetObj.gameObject.tag == "Enemy")
        {
            Destroy(targetObj.gameObject);
        }
    }
}
