using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;

public class scr_MovementJoystick : MonoBehaviour
{
    Mudcake_actions myControls;
    private Vector2 mousePosition;

    public GameObject joystick;
    public GameObject joystickBG;
    public Vector2 joystickVec;
    private Vector2 joystickTouchPos;
    private Vector2 joystickOriginalPos;
    private float joystickRadius;

    private void Awake()
    {
        myControls = new Mudcake_actions();
        myControls.Minigames.MousePosition.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
    }

    void Start()
    {

        joystickOriginalPos = joystickBG.transform.position;
        joystickRadius = joystick.GetComponent<RectTransform>().sizeDelta.y / 4;
    }

    public void PointerDown()
    {
        joystick.transform.position = mousePosition;
        joystickBG.transform.position = mousePosition;
        joystickTouchPos = mousePosition;
    }

    public void Drag(BaseEventData baseEventData)
    {
        PointerEventData pointerEventData = baseEventData as PointerEventData;
        Vector2 dragPos = pointerEventData.position;
        joystickVec = (dragPos - joystickTouchPos).normalized;

        float joystickDist = Vector2.Distance(dragPos, joystickTouchPos);

        if(joystickDist < joystickRadius)
        {
            joystick.transform.position = joystickTouchPos + joystickVec * joystickDist;
        }

        else
        {
            joystick.transform.position = joystickTouchPos + joystickVec * joystickRadius;
        }
    }
    public void PointerUp()
    {
        joystickVec = Vector2.zero;
        joystickVec.y = 0;
        joystick.transform.position = joystickOriginalPos;
        joystickBG.transform.position = joystickOriginalPos;
    }

    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }

}
