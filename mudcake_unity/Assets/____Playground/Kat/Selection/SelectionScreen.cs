using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SelectionScreen : MonoBehaviour
{
    public GameObject CanvasEvolve;
    public GameObject CanvasChoice;
    public static bool Spike = false;
    public static bool Round = false;



   


//here are input system stuff-->

Mudcake_actions myControls;

    private void Awake()
    {
        myControls = new Mudcake_actions();
        myControls.Minigames.Qkey.performed += ctx => q();
        myControls.Minigames.Wkey.performed += ctx => w();
    }



    // here starts the creature evolution code

    void Start()
    {
   
    }
    void Update()
    {
        

    }



    //here are input system stuff again-->
    private void q()
    {
        if (CanvasChoice.activeSelf) {
            CanvasEvolve.SetActive(false);
            CanvasChoice.SetActive(false);
        }
        else
        {
            Round = true;
            Debug.Log(Round);
            SceneManager.LoadScene("Creature");
        }

    }

    private void w()
    {
        if (CanvasChoice.activeSelf)
        {
            CanvasEvolve.SetActive(true);
            CanvasChoice.SetActive(false);
        }
        else
        {
            Spike = true;
            Debug.Log(Spike);
            SceneManager.LoadScene("Creature");
        }
    }

    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }

}
