using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SpawOnCreature : MonoBehaviour
{
    
    GameObject objectToSpaw;
    public float amountObject;
    //public float PlanetRadius;
    public GameObject PlanetOrigin;
    public GameObject Creature;
    public float spawCollisionCheckRadius;
    float CretureR;
    float CreatureS;
    public List<SOObjectToSpawn> objectsToSpawn = new List<SOObjectToSpawn>();
    public GameObject CubeToSpaw;
    public GameObject SphereToSpaw;
    public List<GameObject> allSpawnedObjects = new List<GameObject>();
    private CreatureController myCreatureController;
    public GameObject cheatStuff;




    //here are input system stuff-->
    Mudcake_actions myControls;

    private void Awake()
    {
        myCreatureController = transform.parent.GetComponent<CreatureController>();
        myControls = new Mudcake_actions();
        myControls.Minigames.Qkey.performed += ctx => q();
        myControls.Minigames.Wkey.performed += ctx => w();
    }



    // here starts the creature evolution code
    void Start()
    {
        objectToSpaw = CubeToSpaw;
    }

    void SpawnObj()
    {
        CretureR = Creature.transform.localScale.x / 2;
        CreatureS = Creature.transform.localScale.x;
        for (int i = 0; i < amountObject; i++)
        {
            Vector3 spawnPoint = Random.onUnitSphere * CretureR + Creature.transform.position;
            //spawCollisionCheckRadius = objectToSpaw.transform.localScale.x / 2;

            if (!Physics.CheckSphere(spawnPoint, spawCollisionCheckRadius))
            {
                GameObject newGO = Instantiate(objectToSpaw, spawnPoint, Quaternion.identity);
                
                //scale the object according to food value
                Vector3 objectScale = newGO.transform.localScale;
                float foodValue = myCreatureController.foodValue;
                float newScale = Map(foodValue, 0, 100, 0.2f, 1.1f);
                newGO.transform.localScale = new Vector3(newScale, newScale, newScale);
                
                allSpawnedObjects.Add(newGO);
                newGO.transform.LookAt(PlanetOrigin.transform);
                //newGO.transform.localScale = newGO.transform.localScale * CreatureS;
                newGO.transform.parent = gameObject.transform;
            }
            else {
                Invoke("SpawnObj", 1);
            }
        }
    }

    public void SpawnNewObject(int objectIndex)
    {
        objectToSpaw = objectsToSpawn[objectIndex].myModel;
        SpawnObj();
    }

    public void ResetCreature()
    {
        foreach (GameObject decoration in allSpawnedObjects)
        {
            Destroy(decoration);
        }
        allSpawnedObjects.Clear();
    }

    //cheat stuff
    public void q()
    {
        cheatStuff.SetActive(!cheatStuff.activeSelf);
    }

    public void w()
    {
        myCreatureController.nextObjectToSpawn = Random.Range(0, objectsToSpawn.Count-1);
        myCreatureController.SpawnNewObject();
    }

    private void OnEnable()
    {
        myControls.Minigames.Enable();
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
    }
    
    private float Map(float input, float inputMin, float inputMax, float min, float max)
    {
        return min + (input - inputMin) * (max - min) / (inputMax - inputMin);
    }

}
