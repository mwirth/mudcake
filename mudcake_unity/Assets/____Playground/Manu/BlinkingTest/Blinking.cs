using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using UnityEngine.UI;

public class Blinking : MonoBehaviour
{
    public Image myBackground;

    private TweenBase myBase;

    private bool isBlinking;
    // Start is called before the first frame update
    void Start()
    {
        isBlinking = true;
        //myBase = Tween.Value(new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 0), new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 1), UpdateColor, 2.5f, 0, Tween.EaseLinear, Tween.LoopType.PingPong);
        StartCoroutine(BlinkingStuff());
    }

    void UpdateColor(Color myColor)
    {
        myBackground.color = myColor;
    }

    private IEnumerator BlinkingStuff()
    {
        while (isBlinking)
        {
            Tween.Value(new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 0),
                new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 1), UpdateColor,
                2f, 0, Tween.EaseLinear);
            yield return new WaitForSeconds(1);
            Tween.Value(new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 1),
                new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 0), UpdateColor,
                2f, 0, Tween.EaseLinear);
            yield return new WaitForSeconds(1);
        }

        yield return null;
    }

    public void StopCurrentTween()
    {
        isBlinking = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }
}
