using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.InputSystem.EnhancedTouch;

public class IntroPlayerInputManager : MonoBehaviour
{
    class FingerData
    {
        public Vector2 StartScreenPosition;
        public Vector2 ScreenPosition;
        public bool Dragging;
        public Transform DraggedTransform;
        public Plane DraggingPlane;
    }
    
    public GameObject cellClone;
    public Transform originCell;
    private List<GameObject> allCells = new List<GameObject>();
    public GameObject progressBar;

    private Mudcake_actions m_Controls;
    private Vector2 mousePosition;
    private Camera mainCamera;

    private Vector2 clickPosition;
    private float dragDistance;
    public float minDragDistance;
    
    private RaycastHit hitClick;
    private RaycastHit hitDrag;

    private bool mainCellHit;
    
    private List<FingerData> allFingers = new List<FingerData>();

    private WorldSH sound;

    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        
        mainCellHit = false;
        mainCamera = Camera.main;
        m_Controls = new Mudcake_actions();
        EnhancedTouchSupport.Enable();
        //m_Controls.Cellparting.SpawnNewCell.performed += ctx => mousePosition = ctx.ReadValue<Vector2>();
        //m_Controls.Cellparting.ClickCell.performed += ctx => OnHoldClick();
        // m_Controls.Cellparting.ClickCell.started += ctx => OnClick();
    }

    private void CheckDragDistance(int index)
    {
        FingerData data = allFingers[index];
        dragDistance = Vector2.Distance(data.StartScreenPosition, data.ScreenPosition);
    }

    
    private void OnClick(int index)
    {
        FingerData data = allFingers[index];

        data.StartScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;

        clickPosition = mousePosition;
        var rayClick = mainCamera.ScreenPointToRay((data.ScreenPosition));
        if (Physics.Raycast(rayClick, out hitClick))
        {
            if (hitClick.collider.CompareTag("OriginCell"))
            {
                data.Dragging = true;
            }
        }
    }

    private void OnTouchMove(int index)
    {
        if (index > 3)
            return;

        FingerData data = allFingers[index];

        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
        CheckDragDistance(index);
        if (dragDistance > minDragDistance && data.Dragging)
        {
            var ray = mainCamera.ScreenPointToRay(data.ScreenPosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                var dragClick = mainCamera.ScreenPointToRay(data.ScreenPosition);
                Physics.Raycast(dragClick, out hitDrag);
                GameObject newCell = Instantiate(cellClone, hit.point, Quaternion.identity, originCell);
                allCells.Add(newCell);
                float allCellsAmount = allCells.Count;
                if (allCellsAmount <= 50) progressBar.transform.localScale = new Vector3(allCellsAmount/50, 1, 1);
                Debug.Log(allCells.Count);
                data.Dragging = false;
                sound.CellPullingApart(newCell.transform);
            }
        }
    }
    private void OnHoldClick(int index)
    {
        Debug.Log("holding");
        if (dragDistance > minDragDistance && mainCellHit)
        {
            Debug.Log("dragging");
            var ray = mainCamera.ScreenPointToRay(mousePosition);
            RaycastHit hit;
            Debug.Log(Physics.Raycast(ray, out hit));
            if (Physics.Raycast(ray, out hit))
            {
                var dragClick = mainCamera.ScreenPointToRay(mousePosition);
                Physics.Raycast(dragClick, out hitDrag);
                Instantiate(cellClone, hit.point, Quaternion.identity, originCell);
                mainCellHit = false;
            }

            
        }
    }
    
    private void Touch_onFingerDown(Finger finger)
    {
        Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count);
        OnClick(finger.index);
    }
    
    private void Touch_onFingerMove(Finger finger)
    {
        OnTouchMove(finger.index);
    }

    private void OnEnable()
    {
        m_Controls.Cellparting.Enable();
        
        TouchSimulation.Enable();

        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove += Touch_onFingerMove;
        // UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += Touch_onFingerUp;
    }

    private void OnDisable()
    {
        m_Controls.Cellparting.Disable();
        
        TouchSimulation.Disable();
        
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove -= Touch_onFingerMove;
        // UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= Touch_onFingerUp;
    }
}
