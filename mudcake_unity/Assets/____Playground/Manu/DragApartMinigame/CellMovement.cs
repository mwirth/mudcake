using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CellMovement : MonoBehaviour
{
    public Vector3 moveDirection;
    private Vector3 grav;
    private Vector3 dir;
    private Rigidbody rb;
    
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float distance = Vector3.Distance(Vector3.up, transform.position);
        if (distance < 3f)
        {
            dir = Vector3.zero;
            grav = Vector3.zero;
            moveDirection = Vector3.zero;
            rb.velocity = Vector3.zero;
        }
        else
        {
            grav = Vector3.up - transform.position;
            dir = grav.normalized;
            moveDirection += dir * 0.4f;
        }
    }

    private void FixedUpdate()
    {
        transform.Translate(moveDirection * Time.deltaTime);
    }

}
