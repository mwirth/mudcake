using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveSelfFromList : MonoBehaviour
{
    private ServerNetworkController myServerController;

    private void Awake()
    {
        myServerController = GameObject.FindWithTag("ServerController").GetComponent<ServerNetworkController>();
    }

    public void RemoveSelf()
    {
        myServerController.reanimationButtons.Remove(gameObject);
        Destroy(gameObject);
    }
}
