using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MLAPI;
using MLAPI.Connection;
using UnityEngine.Networking.Types;
using MLAPI.Messaging;

public class ButtonLogic : MonoBehaviour
{
    public void ActivateButtonEvent()
    {
        if (NetworkManager.Singleton.ConnectedClients.TryGetValue(NetworkManager.Singleton.LocalClientId,
            out var networkedClient))
        {
            var player = networkedClient.PlayerObject.GetComponent<PlayerNetworkController>();
            player.ButtonPressed();
        }
        
    }
}
