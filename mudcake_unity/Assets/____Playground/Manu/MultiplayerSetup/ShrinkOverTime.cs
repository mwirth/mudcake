using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShrinkOverTime : MonoBehaviour
{
    public float shrinkSpeed = 1;
    private ReanimateLogic _reanimateLogic;
    
    // Start is called before the first frame update
    void Start()
    {
        _reanimateLogic = GetComponentInParent<ReanimateLogic>();
    }

    // Update is called once per frame
    void Update()
    {
        transform.localScale -= new Vector3(0.1f, 0.1f, 0.1f) * Time.deltaTime * shrinkSpeed;
        if (transform.localScale.x < 0)
        {
            //lost 
            gameObject.SetActive(false);
            _reanimateLogic.ChangeCircleScale(-0.5f);
        }
    }
}
