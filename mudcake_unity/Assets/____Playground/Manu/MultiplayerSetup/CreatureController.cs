using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using MLAPI.Connection;
using MLAPI.NetworkVariable;
using UnityEngine;
using MLAPI.Messaging;
using Pixelplacement;
using UnityEngine.UI;

public class CreatureController : NetworkBehaviour
{
    public SpawOnCreature spawnerScript;
    public float spawnedAmount = 0;

    public int nextObjectToSpawn;

    public float heartValue;
    public Slider waterSlider;
    public float waterValue;
    public Slider foodSlider;
    public float foodValue;
    public Slider emotionSlider;
    public float emotionValue;

    private float startScale;

    public Material plantShader;
    public ParticleSystem sparclesSystem;

    private WorldSH sound;
    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        startScale = transform.localScale.x;
        SetPlantColor();
        waterSlider.onValueChanged.AddListener(delegate(float arg0) {UpdateValueWater();  });
        foodSlider.onValueChanged.AddListener(delegate(float arg0) {UpdateValueFood();  });
        emotionSlider.onValueChanged.AddListener(delegate(float arg0) {UpdateValueEmotion();  });
    }

    public void SetNextObjectToSpawn(int number)
    {
        nextObjectToSpawn = number;
    }
    public void SpawnNewObject()
    {
        spawnerScript.SpawnNewObject(nextObjectToSpawn);
        sound.GrowingBodypart(transform);
        spawnedAmount += 0.2f;
        sound.Update_Calm_Music(spawnedAmount);
    }

    public void SetSize()
    {
        float value = startScale - 2.5f + foodValue / 20;
        Tween.LocalScale(transform, new Vector3(value, value, value), 0.5f, 0.2f);
        //transform.localScale = new Vector3(value, value, value);
    }

    public void SetPlantColor()
    {
        float waterTemp = waterValue * -1;
        float plantFloat = Map(waterTemp, -100, 0, 0, 0.7f);
        plantShader.SetFloat(Shader.PropertyToID("_ColorChange"), plantFloat);
    }

    public void SetSparcles()
    {
        var myEmission = sparclesSystem.emission;
        myEmission.rateOverTime = emotionValue;
    }
    
    private float Map(float input, float inputMin, float inputMax, float min, float max)
    {
        return min + (input - inputMin) * (max - min) / (inputMax - inputMin);
    }

    public void UpdateValueWater()
    {
        waterValue = waterSlider.value;
        SetPlantColor();
    }
    
    public void UpdateValueFood()
    {
        foodValue = foodSlider.value;
    }
    
    public void UpdateValueEmotion()
    {
        emotionValue = emotionSlider.value;
        SetSparcles();
    }

}
