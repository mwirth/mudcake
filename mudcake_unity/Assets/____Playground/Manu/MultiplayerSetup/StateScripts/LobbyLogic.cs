using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;

public class LobbyLogic : State
{
    private void OnEnable()
    {
        Debug.Log("Lobby loaded");
    }

    private void OnDisable()
    {
        Debug.Log("Game started");
    }
}
