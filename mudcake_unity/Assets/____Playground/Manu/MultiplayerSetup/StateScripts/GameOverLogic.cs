using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
public class GameOverLogic : State
{
    public SpawOnCreature mySpawnController;
    public Camera myCamera;
    public WorldSH sound;
    public bool iAmDeathScreen = false;
    private void OnEnable()
    {
        //throw new NotImplementedException();
        Show();
        if (iAmDeathScreen)
        {
            sound.Update_Music(6);
            sound.Flatline(sound.transform);
        }
        else sound.Update_Music(4);
        if (!iAmDeathScreen) sound.CreatureRelease(sound.transform);
    }

    private void OnDisable()
    {
        mySpawnController.ResetCreature();
    }
    
    // Turn on the bit using an OR operation:
    private void Show() {
        myCamera.cullingMask = -1;
    }
     
    // Turn off the bit using an AND operation with the complement of the shifted int:
    private void Hide() {
        myCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer("CreatureCam"));
    }
     
    // Toggle the bit using a XOR operation:
    private void Toggle() {
        myCamera.cullingMask ^= 1 << LayerMask.NameToLayer("CreatureCam");
    }
}
