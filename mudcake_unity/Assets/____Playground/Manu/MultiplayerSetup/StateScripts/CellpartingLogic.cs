using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using Unity.Mathematics;
using UnityEngine;

public class CellpartingLogic : State
{
    private GameObject activeIntro;
    public float cellpartingDuration;
    public GameObject cellPartingGame;
    private StateMachine myStateMachine;
    public GameObject timerBar;
    public ServerNetworkController myServerController;

    private WorldSH sound;

    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        myStateMachine = transform.parent.GetComponent<StateMachine>();
    }

    private void OnEnable()
    {
        sound.Update_Music(1);
        activeIntro = Instantiate(cellPartingGame, Vector3.zero, quaternion.identity, transform);
        StartCoroutine(CountDown());
    }

    private void OnDisable()
    {
        Destroy(activeIntro);
        sound.LifeformAppears(sound.transform);
        myServerController.ReminderCallClientRpc();
    }

    IEnumerator CountDown()
    {
        float timer = cellpartingDuration;
        while (timer > 0)
        {
            timerBar.transform.localScale = new Vector3(timer / cellpartingDuration, timerBar.transform.localScale.y,
                timerBar.transform.localScale.y);
            yield return new WaitForSeconds(1);
            timer--;
        }
        myStateMachine.ChangeState("CalmPhaseServer");
    }
}