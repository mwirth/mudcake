using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using UnityEngine;
using Pixelplacement;
using UnityEngine.UI;
using TMPro;

public class ClientCanvasLogic : State
{
    public int playerNumber;
    public GameObject myButtonObject;
    private Image myButtonImage;
    private TMP_Text myButtonText;
    private Button myButton;
    public Image myBackground;
    public Color playerColorBackground;
    public Color p1Color;
    public Color p2Color;
    public Color p3Color;
    public Color p4Color;
    private bool isInteractable;
    public GameObject valueBar;
    private Image valueBarImage;
    public TMP_Text descriptionText;
    public List<GameObject> spheres = new List<GameObject>();
    private void OnEnable()
    {
        valueBarImage = valueBar.transform.GetChild(0).gameObject.GetComponent<Image>();
        Debug.Log(Convert.ToInt32(NetworkManager.Singleton.LocalClientId - 1));
        playerNumber = Convert.ToInt32(NetworkManager.Singleton.LocalClientId - 1);
        myButtonImage = myButtonObject.GetComponent<Image>();
        //myButtonText = myButtonObject.gameObject.GetComponentInChildren<TMP_Text>();
        myButton = myButtonObject.GetComponent<Button>();
        PlayerScreenSetup(playerNumber);
    }

    public void PlayerScreenSetup(int myPlayerNumber)
    {
        playerNumber = myPlayerNumber;
        if (myPlayerNumber == 1)
        {
            spheres[0].SetActive(true);
            valueBarImage.color = p1Color;
            isInteractable = false;
            //myButtonText.enabled = false;
            descriptionText.SetText("You are the HEART. You beat. Everything depends on you.");
        }
        if (myPlayerNumber == 2)
        {
            spheres[1].SetActive(true);
            valueBarImage.color = p2Color;
            isInteractable = false;
            //myButtonText.enabled = false;
            descriptionText.SetText("You are responsible for the NUTRITION. Feed the creature.");
        }
        if (myPlayerNumber == 3)
        {
            spheres[2].SetActive(true);
            valueBarImage.color = p3Color;
            isInteractable = false;
            //myButtonText.enabled = false;
            descriptionText.SetText("You are responsible for the HYDRATION. Water the creature.");
        }
        if (myPlayerNumber == 4)
        {
            spheres[3].SetActive(true);
            valueBarImage.color = p4Color;
            isInteractable = false;
            //myButtonText.enabled = false;
            descriptionText.SetText("You are responsible for the SANITY. Interact with the creature.");
        }
        
        
        myButton.interactable = isInteractable;
        myBackground.color = playerColorBackground;
    }

    private void OnDisable()
    {
        //throw new NotImplementedException();
    }
}
