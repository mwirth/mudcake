using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class ReanimationLogic : State
{
    public GameObject reanimationMinigame;
    private GameObject reanimObj;
    public WorldSH sound;
    private void OnEnable()
    {
        reanimObj = Instantiate(reanimationMinigame, transform);
        sound.Update_Music(5);
    }

    private void OnDisable()
    {
        Destroy(reanimObj);
    }
    
}
