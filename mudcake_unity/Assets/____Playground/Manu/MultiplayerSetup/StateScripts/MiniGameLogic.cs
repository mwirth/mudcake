using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Random = UnityEngine.Random;

public class MiniGameLogic : State
{
    public float minigameRunningTime;
    public StateMachine myStateMachine;
    private GameObject currentMinigame;
    public List<GameObject> allMinigames = new List<GameObject>();
    public CreatureController myCreatureController;
    public GameObject timerBar;
    public bool chooseMinigame = false;
    public int minigameToChoose;
    private Coroutine runningMinigame;
    public Camera myCamera;
    public WorldSH sound;
    
    private void OnEnable()
    {
        if (chooseMinigame)
        {
            currentMinigame = Instantiate(allMinigames[minigameToChoose], Vector3.zero, Quaternion.identity, transform);
        }
        else
        {
            currentMinigame = Instantiate(allMinigames[Random.Range(0, allMinigames.Count)], Vector3.zero, Quaternion.identity, transform);

        }
        runningMinigame = StartCoroutine(MinigameTimer());
        Hide();
        sound.Update_Music(3);
    }

    private void OnDisable()
    {
        scr_GameControllMultitouch myMinigameController = currentMinigame.GetComponent<scr_GameControllMultitouch>();
        if (myMinigameController != null)
        {
            foreach (GameObject enemy in myMinigameController.allEnemies)
            {
                Destroy(enemy);
            }
            myMinigameController.allEnemies.Clear();
        }
        Destroy(currentMinigame);
    }

    public void LoseMinigame()
    {
        StopCoroutine(runningMinigame);
        sound.NegativFeedback(sound.transform);
        myStateMachine.ChangeState("CalmPhaseServer");
    }

    IEnumerator MinigameTimer()
    {
        float timer = minigameRunningTime;
        while (timer > 0)
        {
            timerBar.transform.localScale = new Vector3(timer / minigameRunningTime, timerBar.transform.localScale.y,
                timerBar.transform.localScale.y);
            yield return new WaitForSeconds(1);
            timer--;
        }
        myCreatureController.SpawnNewObject();
        sound.GoodFeedback(sound.transform);
        myStateMachine.ChangeState("CalmPhaseServer");
    }

    // Turn off the bit using an AND operation with the complement of the shifted int:
    private void Hide() {
        myCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer("CreatureCam"));
    }
}
