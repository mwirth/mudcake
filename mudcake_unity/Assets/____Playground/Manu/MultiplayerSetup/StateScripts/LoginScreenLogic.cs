using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;

public class LoginScreenLogic : State
{
    public Camera myCamera;
    private void OnEnable()
    {
        Hide();
        Debug.Log("Login Screen loaded");
    }

    private void OnDisable()
    {
        Debug.Log("Game started");
    }

    // Turn off the bit using an AND operation with the complement of the shifted int:
    private void Hide() {
        myCamera.cullingMask &=  ~(1 << LayerMask.NameToLayer("CreatureCam"));
    }
}
