using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using TMPro;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class ChooseEvolutionLogic : State
{
    public GameObject optionAButton;
    public GameObject optionBButton;
    public GameObject optionCButton;
    private Image optAImg;
    private Image optBImg;
    private Image optCImg;
    private TMP_Text buttonTextA;
    private TMP_Text buttonTextB;
    private TMP_Text buttonTextC;
    private int optionAint;
    private int optionBint;
    private int optionCint;
    public Transform objALocation;
    public Transform objBLocation;
    public Transform objCLocation;

    public CreatureController myCreatureController;
    public SpawOnCreature creatureSpawner;

    private List<int> indexes = new List<int>();

    private void Awake()
    {
        buttonTextA = optionAButton.GetComponentInChildren<TMP_Text>();
        buttonTextB = optionBButton.GetComponentInChildren<TMP_Text>();
        buttonTextC = optionCButton.GetComponentInChildren<TMP_Text>();
        optAImg = optionAButton.GetComponent<Image>();
        optBImg = optionBButton.GetComponent<Image>();
        optCImg = optionCButton.GetComponent<Image>();
    }

    private void OnEnable()
    {
        indexes.Clear();
        for (int i = 0; i < creatureSpawner.objectsToSpawn.Count; i++)
        {
            indexes.Add(i);
        }

        optionAint = SetOptionsToChooseFrom(buttonTextA, optAImg);
        optionBint = SetOptionsToChooseFrom(buttonTextB, optBImg);
        optionCint = SetOptionsToChooseFrom(buttonTextC, optCImg);

    }

    private void OnDisable()
    {
        //throw new NotImplementedException();
    }

    private int SetOptionsToChooseFrom(TMP_Text buttonText, Image changeImage)
    {
        int randomInt = Random.Range(0, indexes.Count);
        Debug.Log(randomInt);
        buttonText.text = creatureSpawner.objectsToSpawn[randomInt].myName;
        changeImage.sprite = creatureSpawner.objectsToSpawn[randomInt].myButtonImage;
        return randomInt;
    }

    public void OnButtonAPress()
    {
        myCreatureController.SetNextObjectToSpawn(optionAint);
    }
    
    public void OnButtonBPress()
    {
        myCreatureController.SetNextObjectToSpawn(optionBint);

    }
    
    public void OnButtonCPress()
    {
        myCreatureController.SetNextObjectToSpawn(optionCint);

    }
}
