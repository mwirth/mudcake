using System;
using System.Collections;
using System.Collections.Generic;
using Pixelplacement;
using UnityEngine;

public class CalmPhaseServerLogic : State
{
    public ServerNetworkController myServercontroller;
    public Camera myCamera;
    public WorldSH sound;
    private void OnEnable()
    {
        myServercontroller.HeartButtonPressedClientRpc();
        Show();
        sound.Update_Music(2);
    }

    private void OnDisable()
    {
        //throw new NotImplementedException();
    }
    
    // Turn on the bit using an OR operation:
    private void Show()
    {
        myCamera.cullingMask = -1;
    }
}
