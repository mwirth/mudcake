using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using MLAPI;
using MLAPI.Connection;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using MLAPI.NetworkVariable.Collections;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class PlayerNetworkController : NetworkBehaviour
{
    [Header("Server owned Objects")]
    private ServerNetworkController myServerController;
    //REMOVE if there is a client only build!
    //private Renderer theSphere;
    
    public int myPlayerNumber;

    [Header("UI Elements")]
    private StateMachine clientServerUIStateMachine;

    private Image myBackground;
    private GameObject valueBar;

    private float heartStartValue;
    private bool isBlinking;
    private Coroutine blinkingCoroutine;

    //Network Variables - Survival Values of each player
    [SerializeField]
    private NetworkVariableFloat survivalValue = new NetworkVariableFloat(new NetworkVariableSettings{WritePermission = NetworkVariablePermission.Everyone},100);
    public NetworkVariableFloat SurvivalValue => survivalValue;

    private WorldSH sound;

    void OnSurvivalValueChanged(float oldValue, float newValue)
    {
        SurvivalValueOnCreatureUpdateServerRpc();
        if (IsOwner && IsClient)
        {
            Tween.LocalScale(valueBar.GetComponent<RectTransform>(), new Vector3(newValue / 100, 1, 1), 0.5f, 0, Tween.EaseInOut);
            
            float blinkingSpeed = 20;
            if (survivalValue.Value > 30)
            {
                isBlinking = false;
                blinkingCoroutine = null;
                return;
            }

            if (survivalValue.Value <= 30)
            {
                // Start fast blinking
                blinkingSpeed = 1;
                isBlinking = true;
                if (blinkingCoroutine == null) blinkingCoroutine = StartCoroutine(BlinkingBackground(blinkingSpeed));
            }
        }
    }

    private void UpdateColor(Color blinkingColor)
    {
        myBackground.color = blinkingColor;
    }

    private IEnumerator BlinkingBackground(float blinkingSpeed)
    {
        while (isBlinking)
        {
            Tween.Value(new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 0),
                new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 1), UpdateColor,
                blinkingSpeed, 0, Tween.EaseInOut);
            yield return new WaitForSeconds(blinkingSpeed);
            Tween.Value(new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 1),
                new Color(myBackground.color.r, myBackground.color.g, myBackground.color.b, 0), UpdateColor,
                blinkingSpeed, 0, Tween.EaseInOut);
            yield return new WaitForSeconds(blinkingSpeed);
        }
    }

    public void ChangeSurvivalValue(float valueChange)
    {
        survivalValue.Value += valueChange;
        if (survivalValue.Value > 100) survivalValue.Value = 100;
        if (survivalValue.Value < 0)
        {
            ValueDroppedBelow0ServerRpc(myPlayerNumber);
            survivalValue.Value = 0;
        }
    }

    public void SetSurvivalValue(float targetValue)
    {
        survivalValue.Value = targetValue;
    }

    public void ChangeHeartValue(float changeValue, float heartStartTime)
    {
        survivalValue.Value = heartStartValue / heartStartTime * changeValue;
    }

    private void OnEnable()
    {
        SurvivalValue.OnValueChanged += OnSurvivalValueChanged;
    }

    private void OnDisable()
    {
        SurvivalValue.OnValueChanged -= OnSurvivalValueChanged;
    }

    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        myServerController = GameObject.FindWithTag("ServerController").GetComponent<ServerNetworkController>();
        
        myPlayerNumber = Convert.ToInt32(NetworkManager.Singleton.LocalClientId) - 1;
        
        clientServerUIStateMachine = GameObject.FindWithTag("ServerClientUIStates").GetComponent<StateMachine>();
        
        //REMOVE if there is a client only build!
        //theSphere = GameObject.FindWithTag("Creature").GetComponent<Renderer>();
        
        heartStartValue = survivalValue.Value;
        
        //do things if you are a client only
        if (IsClient && !IsServer)
        {
            clientServerUIStateMachine.ChangeState("ClientCanvas");
            myBackground = clientServerUIStateMachine.GetComponentInChildren<ClientCanvasLogic>().myBackground;
            valueBar = clientServerUIStateMachine.GetComponentInChildren<ClientCanvasLogic>().valueBar;
            //REMOVE if there is a client only build!
            //theSphere.enabled = false;
        }
        if (IsServer) PlayerLogsIn();
    }

    //Button logic local on client
    public void ButtonPressed()
    {
        ButtonPressedServerRpc(myPlayerNumber);
        if (myPlayerNumber == 1) sound.PhoneHeartpump(sound.transform);
        if (myPlayerNumber == 2) sound.PhoneGrowing(sound.transform);
        if (myPlayerNumber == 3) sound.PhoneWatering(sound.transform);
        if (myPlayerNumber == 4) sound.PhoneAddedHappyParticle(sound.transform);
    }
   
    //Code executed on the Server
    [ServerRpc(RequireOwnership = false)]
    private void ButtonPressedServerRpc(int playerNumber)
    {
        //Player 1 presses Button
        if (playerNumber == 1) myServerController.Player1ButtonPress(playerNumber);

        //Player 2 presses Button
        if (playerNumber == 2) myServerController.Player2ButtonPress(playerNumber);

        //Player 3 presses Button
        if (playerNumber == 3) myServerController.Player3ButtonPress(playerNumber);

        //Player 4 presses Button
        if (playerNumber == 4) myServerController.Player4ButtonPress(playerNumber);
    }

    [ServerRpc(RequireOwnership = false)]
    private void ValueDroppedBelow0ServerRpc(int playerNumber)
    {
        myServerController.StartDyingCoroutine(playerNumber);
    }

    [ServerRpc(RequireOwnership = false)]
    private void SurvivalValueOnCreatureUpdateServerRpc()
    {
        myServerController.SetCurrentSurvivalValues();
    }
    
    
    private void PlayerLogsIn()
    {
        if (!myServerController.loginSigns[0].activeSelf) myServerController.loginSigns[0].SetActive(true);
        else if (!myServerController.loginSigns[1].activeSelf) myServerController.loginSigns[1].SetActive(true);
        else if (!myServerController.loginSigns[2].activeSelf) myServerController.loginSigns[2].SetActive(true);
        else if (!myServerController.loginSigns[3].activeSelf) myServerController.loginSigns[3].SetActive(true);
        myServerController.sound.PhoneJoined(sound.transform);
    }
}
