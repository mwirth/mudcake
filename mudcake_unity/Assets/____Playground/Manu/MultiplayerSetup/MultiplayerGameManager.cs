using System.Collections.Generic;
using FMOD;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.SceneManagement;
using MLAPI.Transports.UNET;
using Pixelplacement;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MultiplayerGameManager : MonoBehaviour
{
    public TMP_InputField inputField;
    public StateMachine myStateMachine;
    public StateMachine uiStateMachine;
    public ServerNetworkController myServerController;
    void OnGUI()
    {
        GUILayout.BeginArea(new Rect(10, 10, 300, 300));
        if (!NetworkManager.Singleton.IsClient && !NetworkManager.Singleton.IsServer)
        {
            //StartButtons();
        }
        else
        {
            StatusLabels();
        }

        GUILayout.EndArea();
    }

    /*static void StartButtons()
    {
        if (GUILayout.Button("Host")) NetworkManager.Singleton.StartHost();
        if (GUILayout.Button("Client")) NetworkManager.Singleton.StartClient();
        if (GUILayout.Button("Server")) NetworkManager.Singleton.StartServer();
    }*/

    static void StatusLabels()
    {
        var mode = NetworkManager.Singleton.IsHost ?
            "Host" : NetworkManager.Singleton.IsServer ? "Server" : "Client";

        GUILayout.Label("Transport: " +
                        NetworkManager.Singleton.NetworkConfig.NetworkTransport.GetType().Name);
        GUILayout.Label("Mode: " + mode);
    }

    public void StartTheHost()
    {
        NetworkManager.Singleton.StartHost();
        myStateMachine.ChangeState("CalmPhaseServer");
    }
    
    public void StartTheClient()
    {
        //NetworkManager.Singleton.GetComponent<UNetTransport>().ConnectAddress = inputField.text;
        NetworkManager.Singleton.StartClient();
        myStateMachine.ChangeState("CalmPhaseClient");
    }
    
    public void StartTheServer()
    {
        NetworkManager.Singleton.StartServer();
        myServerController.sound.Music_MainScore(myServerController.sound.transform);
        myStateMachine.ChangeState("Lobby");
        uiStateMachine.ChangeState("ServerCanvas");
    }
}
