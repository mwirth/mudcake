using System;
using System.Collections;
using System.Collections.Generic;
using FMOD;
using MLAPI;
using MLAPI.Messaging;
using MLAPI.NetworkVariable;
using Pixelplacement;
using Pixelplacement.TweenSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

public class ServerNetworkController : NetworkBehaviour
{
    [Header("Server owned GameObjects")]
    public ParticleSystem waterBubbles;
    public Renderer theCreature;
    public CreatureController myCreatureController;
    
    [Header("UI Elements")]
    public TMP_Text countdownDisplay;
    public Button myButton;
    public TMP_Text myButtonText;
    private int materialIndex;
    public List<GameObject> loginSigns = new List<GameObject>();


    [Header("Countdown Timer")]
    public float heartStarttime;
    private Coroutine heart;
    
    public float foodStarttime;
    private Coroutine food;
    
    public float waterStarttime;
    private Coroutine water;
    
    public float emotionsStarttime;
    private Coroutine emotion;

    [Header("Player Resources")] 
    public List<Material> myMaterials = new List<Material>();

    [Header("Reanimation Phase")]
    public GameObject serverCanvas;
    public GameObject reanimationButton;
    public List<GameObject> reanimationButtons = new List<GameObject>();
    public float reanimationSpawntime;
    public Coroutine dyingCoroutine;
    
    public StateMachine gameLoopStatemachine;

    private Coroutine heartBeatCoroutine = null;

    public WorldSH sound;
    
    //Client calls
    [ClientRpc]
    public void HeartButtonPressedClientRpc()
    {
        myButtonText.enabled = true;
        myButton.interactable = true;
    }

    [ClientRpc]
    public void ReminderCallClientRpc()
    {
        sound.PhoneBeginnPhone(sound.transform);
    }

    [ClientRpc]
    public void DisablePlayerButtonClientRpc(int playerNumber)
    {
        if (Convert.ToInt32(NetworkManager.Singleton.LocalClientId) == playerNumber + 1)
        {
            myButtonText.enabled = false;
            myButton.interactable = false;
        }
    }

    [ClientRpc]
    private void ChangeSurvivalValueClientRpc(int playerNumber, float changeValue)
    {
        if (Convert.ToInt32(NetworkManager.Singleton.LocalClientId) == playerNumber + 1)
        {
            NetworkManager.Singleton.ConnectedClients[NetworkManager.Singleton.LocalClientId].PlayerObject
                .GetComponent<PlayerNetworkController>().ChangeSurvivalValue(changeValue);
            if (changeValue < 0) sound.PhoneShrinking(sound.transform);
        }
    }

    [ClientRpc]
    private void ReanimationSuccessfulClientRpc(int playerNumber)
    {
        ChangeSurvivalValueClientRpc(playerNumber, 50);
        myButtonText.enabled = true;
        myButton.interactable = true;
    }

    [ClientRpc]
    private void ChangeHeartValueClientRpc(int playerNumber, float countdown, float heartStartTime)
    {
        if (Convert.ToInt32(NetworkManager.Singleton.LocalClientId) == playerNumber + 1)
        {
            NetworkManager.Singleton.ConnectedClients[NetworkManager.Singleton.LocalClientId].PlayerObject
                .GetComponent<PlayerNetworkController>().ChangeHeartValue(countdown, heartStartTime);
        }
    }

    [ClientRpc]
    private void ResetSurvivalValuesClientRpc()
    {
        if (IsClient) NetworkManager.Singleton.ConnectedClients[NetworkManager.Singleton.LocalClientId].PlayerObject
            .GetComponent<PlayerNetworkController>().SetSurvivalValue(100);
    }
    
    public void SetCurrentSurvivalValues()
    {
        if (NetworkManager.Singleton.ConnectedClients.Count >= 1) myCreatureController.heartValue = NetworkManager.Singleton.ConnectedClientsList[0].PlayerObject.GetComponent<PlayerNetworkController>().SurvivalValue.Value;
        if (NetworkManager.Singleton.ConnectedClients.Count >= 2) myCreatureController.foodValue = NetworkManager.Singleton.ConnectedClientsList[1].PlayerObject.GetComponent<PlayerNetworkController>().SurvivalValue.Value;
        if (NetworkManager.Singleton.ConnectedClients.Count >= 3) myCreatureController.waterValue = NetworkManager.Singleton.ConnectedClientsList[2].PlayerObject.GetComponent<PlayerNetworkController>().SurvivalValue.Value;
        if (NetworkManager.Singleton.ConnectedClients.Count >= 4) myCreatureController.emotionValue = NetworkManager.Singleton.ConnectedClientsList[3].PlayerObject.GetComponent<PlayerNetworkController>().SurvivalValue.Value;
    }

    #region PlayerButtonHandling
    public void Player1ButtonPress(int playerNumber)
    {
        if (heart != null)
        {
            if (heartBeatCoroutine == null) heartBeatCoroutine = StartCoroutine(HeartBeatAnim());
            StopCoroutine(heart);
            if (food != null)
            {
                StopCoroutine(food);
                PunishmentReduceScale();
            }
            if (water != null)
            {
                StopCoroutine(water);
                //punishment effect here
                ChangeSurvivalValueClientRpc(3, -10);
            }
            if (emotion != null)
            {
                StopCoroutine(emotion);
                //punishment effect here
                ChangeSurvivalValueClientRpc(4, -10);
            }
        }

        HeartButtonPressedClientRpc();
        ChangeHeartValueClientRpc(1, heartStarttime, heartStarttime);
        heart = StartCoroutine(HeartCountdown());
        food = StartCoroutine(FoodCountdown());
        water = StartCoroutine(WaterCountdown());
        emotion = StartCoroutine(EmotionCountdown());
        Debug.Log("player "+ 1 + " presses button");

    }
    
    public void Player2ButtonPress(int playerNumber)
    {
        if (food != null) StopCoroutine(food);
        food = null;
        //Reward effect here
        ChangeSurvivalValueClientRpc(playerNumber, 10);
        myCreatureController.SetSize();
        
        //if (theCreature.gameObject.transform.localScale.x < 3) theCreature.gameObject.transform.localScale += new Vector3(1, 1, 1)/10;
        DisablePlayerButtonClientRpc(2);
        Debug.Log("player "+ 2 + " presses button");
    }
    
    public void Player3ButtonPress(int playerNumber)
    {
        if (water != null) StopCoroutine(water);
        water = null;
        waterBubbles.Play();
        myCreatureController.SetPlantColor();
        
        //Reward effect here
        ChangeSurvivalValueClientRpc(playerNumber, 10);
        DisablePlayerButtonClientRpc(3);
        Debug.Log("player "+ 3 + " presses button");
    }
    
    public void Player4ButtonPress(int playerNumber)
    {
        if (emotion != null) StopCoroutine(emotion);
        emotion = null;
        myCreatureController.SetSparcles();
        
        //Reward effect here   
        ChangeSurvivalValueClientRpc(playerNumber, 10);
        DisablePlayerButtonClientRpc(4);
        Debug.Log("player "+ 4 + " presses button");
    }
    
    
    //Player 1 - Coroutine and Functions
    IEnumerator HeartBeatAnim()
    {
        Transform creatureTransform = theCreature.transform.parent.transform;
        Tween.LocalScale(creatureTransform, creatureTransform.localScale + 0.3f * creatureTransform.localScale, 0.2f, 0, Tween.EaseInBack);
        Tween.LocalScale(creatureTransform, creatureTransform.localScale, 0.2f, 0.2f, Tween.EaseOutBack);
        yield return new WaitForSeconds(0.2f);
        heartBeatCoroutine = null;
    }
    IEnumerator HeartCountdown()
    {
        float countdown = heartStarttime;
        countdownDisplay.text = countdown.ToString();
        while (countdown > 0)
        {
            yield return new WaitForSeconds(1);
            countdown -= 1;
            ChangeHeartValueClientRpc(1, countdown, heartStarttime);
            countdownDisplay.text = countdown.ToString();
        }
        //Start Reanimation!
        StartDyingCoroutine(1);
        yield return null;
    }
    
    //Player 2 - Coroutine and Functions
    IEnumerator FoodCountdown()
    {
        float countdown = foodStarttime;
        while (countdown > 0)
        {
            yield return new WaitForSeconds(1);
            countdown -= 1;
        }
        PunishmentReduceScale();
        food = null;
        yield return null;
    }
    
    private void PunishmentReduceScale()
    {
        //if (theCreature.gameObject.transform.localScale.x > 2) theCreature.gameObject.transform.localScale -= new Vector3(1, 1, 1)/10;
        DisablePlayerButtonClientRpc(2);
        ChangeSurvivalValueClientRpc(2, -10);
        myCreatureController.SetSize();
    }
    
    //Player 3 - Coroutine and Functions
    IEnumerator WaterCountdown()
    {
        float countdown = waterStarttime;
        while (countdown > 0)
        {
            yield return new WaitForSeconds(1);
            countdown -= 1;
        }
        myCreatureController.SetPlantColor();
        ChangeSurvivalValueClientRpc(3, -10);
        DisablePlayerButtonClientRpc(3);
        water = null;
        yield return null;
    }
    
    //Player 4 - Coroutine and Functions
    IEnumerator EmotionCountdown()
    {
        float countdown = emotionsStarttime;
        while (countdown > 0)
        {
            yield return new WaitForSeconds(1);
            countdown -= 1;
        }
        myCreatureController.SetSparcles();
        ChangeSurvivalValueClientRpc(4, -10);
        DisablePlayerButtonClientRpc(4);
        emotion = null;
        yield return null;
    }
    #endregion
    
    //Creature is Dying
    public void StartDyingCoroutine(int playerNumber)
    {
        if (dyingCoroutine == null)
        {
            StopAllCoroutines();
            DisablePlayerButtonClientRpc(1);
            DisablePlayerButtonClientRpc(2);
            DisablePlayerButtonClientRpc(3);
            DisablePlayerButtonClientRpc(4);
            
            StopAllCoroutines();
            if (heart != null) StopCoroutine(heart);
            heart = null;
            if (food != null) StopCoroutine(food);
            food = null;
            if (water != null) StopCoroutine(water);
            water = null;
            if (emotion != null) StopCoroutine(emotion);
            emotion = null;
            //dyingCoroutine = StartCoroutine(CreatureIsDying(playerNumber));
            gameLoopStatemachine.ChangeState("Reanimation");
        }
    }
    public IEnumerator CreatureIsDying(int playerNumber)
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject newReanimButton = Instantiate(reanimationButton, Vector3.zero, Quaternion.identity, serverCanvas.transform);
            newReanimButton.GetComponent<RectTransform>().position =
                new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), 0);
            reanimationButtons.Add(newReanimButton);
        }
        
        yield return new WaitForSeconds(reanimationSpawntime);
        
        for (int i = 0; i < 20; i++)
        {
            if (reanimationButtons.Count <= 0)
            {
                ReanimationSuccessfulClientRpc(playerNumber);
                gameLoopStatemachine.ChangeState("CalmPhaseServer");
                HeartButtonPressedClientRpc();
                dyingCoroutine = null;
                yield break;
            }
            GameObject newReanimButton = Instantiate(reanimationButton, Vector3.zero, Quaternion.identity, serverCanvas.transform);
            newReanimButton.GetComponent<RectTransform>().position =
                new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), 0);
            reanimationButtons.Add(newReanimButton);
            yield return new WaitForSeconds(reanimationSpawntime);
        }
        
        //Creature Dies - Set all stuff here
        gameLoopStatemachine.ChangeState("GameOverDeath");
        
        foreach (GameObject button in reanimationButtons)
        {
            Destroy(button);
        }
        reanimationButtons.Clear();
        Debug.Log("dead - really dead");
        dyingCoroutine = null;
        yield return null;
    }

    #region StateChanging
    //Open Selection Window
    public void OpenSelectionWindow()
    {
        gameLoopStatemachine.ChangeState("ChooseEvolution");
        HeartButtonPressedClientRpc();
    }
    
    //A minigame Starts
    public void StartAMinigame()
    {
        gameLoopStatemachine.ChangeState("Minigame");
    }
    
    //Minigame successfully ended
    public void WinAMinigame()
    {
        gameLoopStatemachine.ChangeState("CalmPhaseServer");
    }

    //Release Creature
    public void ReleaseCreature()
    {
        StopAllCoroutines();
        gameLoopStatemachine.ChangeState("GameOverRelease");
    }

    public void StartTheGame()
    {
        StopAllCoroutines();
        gameLoopStatemachine.ChangeState("Cellparting");
        ResetSurvivalValuesClientRpc();
    }
    
    public void RestartTheGame()
    {
        StopAllCoroutines();
        gameLoopStatemachine.ChangeState("Lobby");
        ResetSurvivalValuesClientRpc();
    }
    
    #endregion
}
