using System;
using System.Collections;
using System.Collections.Generic;
using MLAPI;
using Pixelplacement;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.EnhancedTouch;
using Random = UnityEngine.Random;

public class ReanimateLogic : MonoBehaviour
{
    class FingerData
    {
        public Vector2 ScreenPosition;
    }

    public int amountOfCells = 5;
    public GameObject reanimationCell;
    private List<GameObject> reanimationCells = new List<GameObject>();
    public GameObject UICircle;
    public float shrinkSpeed = 1;
    private StateMachine myStateMachine;

    private Vector3 originalCamPosition;
    public float shakeIntensity = 1;
    private Coroutine shakeRoutine;
    
    //private Vector2 mousePosition;
    private Vector3 objPosition;
    private Camera mainCamera;

    private RaycastHit hitClick;
    //private RaycastHit hitClickEnemy;
    
    //Imputsystem
    Mudcake_actions myControls;
    private int playerID;
    private Vector2 touchPosition1;
    private Vector2 touchPosition2;
    private Vector2 touchPosition3;
    private Vector2 touchPosition4;

    private Vector2 FingerPosition;

    //fingers
    private List<FingerData> allFingers = new List<FingerData>();
    
    private int fingerP1;
    private int fingerP2;
    private int fingerP3;
    private int fingerP4;

    private int Fingerindex;

    private int currentTouchID;

    private WorldSH sound;
    private float reanimValue;
    
    private void Awake()
    {
        sound = GameObject.FindWithTag("Sound").GetComponent<WorldSH>();
        sound.HeartRacing(transform);
        myStateMachine = GetComponentInParent<StateMachine>();
        shakeRoutine = StartCoroutine(CameraShake(shakeIntensity));
        for (int i = 0; i < amountOfCells; i++)
        {
            GameObject newCell = Instantiate(reanimationCell,
                new Vector3(Random.Range(-14, 14), 0, Random.Range(-8, 8)), Quaternion.identity, transform);
            reanimationCells.Add(newCell);
        }
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());
        allFingers.Add(new FingerData());

        EnhancedTouchSupport.Enable();
        mainCamera = Camera.main;
        myControls = new Mudcake_actions();
        originalCamPosition = mainCamera.transform.position;
    }

    private void Update()
    {
        UICircle.transform.localScale -= new Vector3(0.1f, 0.01f, 0.1f) * Time.deltaTime * shrinkSpeed;
        if (UICircle.transform.localScale.x < 2.5f)
        {
            foreach (GameObject cell in reanimationCells)
            {
                Destroy(cell);
            }
            reanimationCells.Clear();
            UICircle.transform.localScale = new Vector3(6, 0.01f, 6);
            mainCamera.transform.position = originalCamPosition;
            StopCoroutine(shakeRoutine);
            sound.StopHeartRacing();
            myStateMachine.ChangeState("GameOverDeath");
        }

        if (UICircle.transform.localScale.x > 14)
        {
            foreach (GameObject cell in reanimationCells)
            {
                Destroy(cell);
            }
            reanimationCells.Clear();   
            UICircle.transform.localScale = new Vector3(6, 0.01f, 6);
            mainCamera.transform.position = originalCamPosition;
            StopCoroutine(shakeRoutine);
            sound.StopHeartRacing();
            myStateMachine.ChangeState("CalmPhaseServer");
        }

        reanimValue = UICircle.transform.localScale.x *-1;
        reanimValue = Map(reanimValue, -14f, 2.5f, 1, 4);
        sound.Update_Reanimation_Music(reanimValue);
    }

    public void ChangeCircleScale(float amount)
    {
        UICircle.transform.localScale += new Vector3(amount, 0, amount);
    }

    private void Touch_onFingerUp(Finger finger)
    {
        
    }

    private void Touch_onFingerMove(Finger finger)
    {
        OnTouchMove(finger.index);
    }

    private void Touch_onFingerDown(Finger finger)
    {
        Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count);
        OnMyClick(finger.index);
    }

    private void OnMyClick(int index)
    {
        if (index > 3)
            return;

        FingerData data = allFingers[index];

        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
        Selection(data);
        Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition);
    }

    private void OnTouchMove(int index)
    {
        if (index > 3)
            return;

        FingerData data = allFingers[index];

        data.ScreenPosition = UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers[index].screenPosition;
    }

    private void Selection(FingerData data)
    {

        var myRay = mainCamera.ScreenPointToRay(data.ScreenPosition);
        Physics.Raycast(myRay, out hitClick);
        
        if (hitClick.collider.CompareTag("Reanimate"))
        {
            sound.ReviveButton(hitClick.collider.transform);
            Debug.Log("reanimating");
            hitClick.collider.gameObject.transform.localScale = new Vector3(1,1,1);
            ChangeCircleScale(0.2f);
        }

    }
    
    private void OnEnable()
    {
        myControls.Minigames.Enable();
        TouchSimulation.Enable();

        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove += Touch_onFingerMove;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp += Touch_onFingerUp;
    }

    private void OnDisable()
    {
        myControls.Minigames.Disable();
        TouchSimulation.Disable();


        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= Touch_onFingerDown;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerMove -= Touch_onFingerMove;
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerUp -= Touch_onFingerUp;
    }
    
    public IEnumerator CameraShake(float magnitude)
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            float x = Random.Range(-1f, 1f) * magnitude;
            float y = Random.Range(-1f, 1f) * magnitude;
            mainCamera.transform.position = originalCamPosition + new Vector3(x, 0, y);
        }
    }
    
    private float Map(float input, float inputMin, float inputMax, float min, float max)
    {
        return min + (input - inputMin) * (max - min) / (inputMax - inputMin);
    }

    
}
