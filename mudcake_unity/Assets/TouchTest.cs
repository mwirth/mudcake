using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UIElements;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem.EnhancedTouch;

public class TouchTest : MonoBehaviour
{
    private Mudcake_actions myControls;
    private int playerID;
    private void Awake()
    {
        myControls = new Mudcake_actions();
        //myControls.TouchActions.Touch.performed += ctx => ;
        EnhancedTouchSupport.Enable();

    }

    void Update()
    {
        
    }

    private void OnMyClick()
    {

        Debug.Log("touching");
      

    }

    private void OnMyRelease()
    {
      
    }


    private void OnEnable()
    {
        myControls.TouchActions.Enable();
        TouchSimulation.Enable();

        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown += FingerDown;
    }

    private void OnDisable()
    {
        myControls.TouchActions.Disable();
        TouchSimulation.Disable();
        
        UnityEngine.InputSystem.EnhancedTouch.Touch.onFingerDown -= FingerDown;
    }

    private void FingerDown(Finger finger)
    {
        Debug.Log(UnityEngine.InputSystem.EnhancedTouch.Touch.activeTouches.Count);
    }

}
