using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldSH : SoundHapticBase
{
    private void Start() 
    {
        //PlayAmbienceSound(1, transform);    
        //PlayForestMusic(2, transform);    
    }
    
    public void Music_MainScore(Transform origin)
    {
        PlaySoundContinuous(soundFiles.Music_MainScore, 2000, origin);
    }

    public void Update_Music(int stageNumber)
    {
        UpdateSound(soundFiles.Music_MainScore, 2000, "whichGameStage", stageNumber);
    }

    public void Update_Calm_Music(float switchNumber)
    {
        UpdateSound(soundFiles.Music_MainScore, 2000, "CalmSwitch", switchNumber);
    }

    public void Update_Reanimation_Music(float reanimAmount)
    {
        UpdateSound(soundFiles.Music_MainScore, 2000, "ReanimationSwitch", reanimAmount);
    }

    public void BlinkingEye(Transform origin)
    {
        PlaySoundOneShot(soundFiles.BlinkingEye, origin);
    }
    
    public void BlopEnemies(Transform origin)
    {
        PlaySoundOneShot(soundFiles.BlopEnemies, origin);
    }
    
    public void BreethingHole(Transform origin)
    {
        PlaySoundOneShot(soundFiles.BreethingHole, origin);
    }
    
    public void CellPullingApart(Transform origin)
    {
        PlaySoundOneShot(soundFiles.CellPullingApart, origin);
    }
    
    public void CollectBad(Transform origin)
    {
        PlaySoundOneShot(soundFiles.CollectBad, origin);
    }
    
    public void CollectGood(Transform origin)
    {
        PlaySoundOneShot(soundFiles.CollectGood, origin);
    }

    public void CollectTentacleBlop(Transform origin)
    {
        PlaySoundOneShot(soundFiles.CollectTentacleBlop, origin);
    }
    
    public void Flatline(Transform origin)
    {
        PlaySoundOneShot(soundFiles.Flatline, origin);
    }
    
    public void GoodFeedback(Transform origin)
    {
        PlaySoundOneShot(soundFiles.GoodFeedback, origin);
    }
    
    public void GrowingBodypart(Transform origin)
    {
        PlaySoundOneShot(soundFiles.GrowingBodypart, origin);
    }
    
    public void HeartRacing(Transform origin)
    {
        PlaySoundContinuous(soundFiles.HeartRacing, 1000, origin);
    }

    public void StopHeartRacing()
    {
        StopSound(soundFiles.HeartRacing, 1000);
    }
    
    public void LifeformAppears(Transform origin)
    {
        PlaySoundOneShot(soundFiles.LifeformAppears, origin);
    }
    public void MovingCell(Transform origin)
    {
        PlaySoundOneShot(soundFiles.MovingCell, origin);
    }
    
    public void MovingGrass(Transform origin)
    {
        PlaySoundOneShot(soundFiles.MovingGrass, origin);
    }
    
    public void NegativFeedback(Transform origin)
    {
        PlaySoundOneShot(soundFiles.NegativFeedback, origin);
    }
    
    public void ReviveButton(Transform origin)
    {
        PlaySoundOneShot(soundFiles.ReviveButton, origin);
    }
    
    public void TentacleGrowing(Transform origin)
    {
        PlaySoundOneShot(soundFiles.TentacleGrowing, origin);
    }
    
    public void TentacleMoving(Transform origin, int index)
    {
        PlaySoundContinuous(soundFiles.TentacleMoving, index, origin);
    }

    public void StopTentacleSound(int index)
    {
        StopSound(soundFiles.TentacleMoving, index);
    }
    
    public void UIClick(Transform origin)
    {
        PlaySoundOneShot(soundFiles.UIClick, origin);
    }
    public void PhoneAddedHappyParticle(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneAddedHappyParticle, origin);
    }
    
    public void PhoneBeginnPhone(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneBeginnPhone, origin);
    }
    
    public void PhoneGrowing(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneGrowing, origin);
    }
    
    public void PhoneHeartpump(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneHeartpump, origin);
    }
    
    public void PhoneJoined(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneJoined, origin);
    }
    
    public void PhoneShrinking(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneShrinking, origin);
    }
    
    public void PhoneWatering(Transform origin)
    {
        PlaySoundOneShot(soundFiles.PhoneWatering, origin);
    }

    public void CreatureRelease(Transform origin)
    {
        PlaySoundOneShot(soundFiles.CreatureRelease, origin);
    }
}
