using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SOSoundDependencies", menuName = "ScriptableObjects/SOSoundDependencies", order = 1)]
public class SOSoundDependencies : ScriptableObject
{
    public FMODUnity.EventReference Music_MainScore, BlinkingEye, BlopEnemies, BreethingHole, CellPullingApart, CollectBad, CollectGood, CollectTentacleBlop, Flatline, GoodFeedback, GrowingBodypart, HeartRacing, LifeformAppears, MovingCell, MovingGrass, NegativFeedback, ReviveButton, TentacleGrowing, TentacleMoving, UIClick, PhoneAddedHappyParticle, PhoneBeginnPhone, PhoneGrowing, PhoneHeartpump, PhoneJoined, PhoneShrinking, PhoneWatering, CreatureRelease;
}
