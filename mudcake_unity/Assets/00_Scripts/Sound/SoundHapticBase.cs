using System.Collections;
using System.Collections.Generic;
using FMOD;
using FMOD.Studio;
using FMODUnity;
using UnityEditor;
using UnityEngine;
using UnityEngine.InputSystem;

public class SoundHapticBase : MonoBehaviour
{   
    #region Sound
    string relAssetPath = "Assets/2. Resources/StreamingAssets/SOSoundDependencies.asset";
    public SOSoundDependencies soundFiles;
    public static Dictionary<int, FMOD.Studio.EventInstance> eventDict = new Dictionary<int, EventInstance>();

    private void Awake()
    {
        if(soundFiles == null)
        {
            #if UNITY_EDITOR
                soundFiles = AssetDatabase.LoadAssetAtPath(relAssetPath, typeof(SOSoundDependencies)) as SOSoundDependencies;
            #endif
        }
        if(soundFiles == null)
            UnityEngine.Debug.LogError("Could not find the sound dependency object!", this);
    }
    
    protected void PlaySoundOneShot(FMODUnity.EventReference eventName, Transform origin = null)
    {
        FMOD.Studio.EventInstance eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventName);
        if(origin != null)
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(eventInstance, origin);
        else
            eventInstance.set3DAttributes(Vector3.zero.To3DAttributes());
        
        eventInstance.start();
        eventInstance.release();
    }

    protected void PlaySoundContinuous(FMODUnity.EventReference eventName, int id, Transform origin = null)
    {
        FMOD.Studio.EventInstance eventInstance = new FMOD.Studio.EventInstance();
        if(eventDict.ContainsKey(id))
        {
            UnityEngine.Debug.Log("Already playing sound.", this);
            return;
        }
        PlaySound(eventName, ref eventInstance, origin);
        eventDict.Add(id, eventInstance);
    }
    protected void UpdateSound(FMODUnity.EventReference eventName, int id, string parameter, float value)
    {
        FMOD.Studio.EventInstance eventInstance;
        if(eventDict.TryGetValue(id, out eventInstance))
            UpdateSound(ref eventInstance, parameter, value);
        else
            UnityEngine.Debug.Log("Not found.");
    }

    protected void StopSound(FMODUnity.EventReference eventName, int id)
    {
        FMOD.Studio.EventInstance eventInstance;
        if(eventDict.TryGetValue(id, out eventInstance))
        {
            StopSound(ref eventInstance);
            UnityEngine.Debug.Log("Stopped " + eventName);
            eventDict.Remove(id);
        }
    }

    void PlaySound(FMODUnity.EventReference eventName, ref FMOD.Studio.EventInstance eventInstance, Transform origin = null)
    {
        eventInstance = FMODUnity.RuntimeManager.CreateInstance(eventName);
        if(origin != null)
            FMODUnity.RuntimeManager.AttachInstanceToGameObject(eventInstance, origin);
        else
            eventInstance.set3DAttributes(Vector3.zero.To3DAttributes());
            
        eventInstance.start();
    }
    void UpdateSound(ref FMOD.Studio.EventInstance eventInstance, string parameter, float value)
    {
        eventInstance.setParameterByName(parameter, value);
    }
    void StopSound(ref FMOD.Studio.EventInstance eventInstance)
    {
        eventInstance.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
    #endregion
}
