//--------------------------------------------------------------------
//
// This is a Unity behaviour script that demonstrates how to build
// a loading screen that can be used at the start of a game to
// load both FMOD Studio data and Unity data without blocking the
// main thread.
//
// To make this work properly "Load All Event Data at Initialization"
// needs to be turned off in the FMOD Settings.
//
// This document assumes familiarity with Unity scripting. See
// https://unity3d.com/learn/tutorials/topics/scripting for resources
// on learning Unity scripting. 
//
//--------------------------------------------------------------------

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SoundLoading : MonoBehaviour
{
    public bool disable;
    // List of Banks to load
    [FMODUnity.BankRef]
    public List<string> Banks;

    // The name of the scene to load and switch to
    public float transitionTime = 1;
    public Animator transition;
    public Image blackScreenImg;
    
    public string firstScene = null;
    public string levelScene = null;
    bool isLoadingScene;
    // GameManager gameManager;
    //
    // public void GetGameManager(GameManager gameManager) => this.gameManager = gameManager;

    public void Start()
    {
        if(disable)
            return;
        blackScreenImg.color = Color.black;
        if(!SceneManager.GetSceneByName(firstScene).isLoaded)
            StartCoroutine(LoadGameAsync());
    }

    IEnumerator LoadGameAsync()
    {
        isLoadingScene = true;
        // Start an asynchronous operation to load the scene
        AsyncOperation async = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        // Don't lead the scene start until all Studio Banks have finished loading
        async.allowSceneActivation = false;

        // Iterate all the Studio Banks and start them loading in the background
        // including the audio sample data
        foreach(var bank in Banks)
        {
            FMODUnity.RuntimeManager.LoadBank(bank, true);
        }

        // Keep yielding the co-routine until all the Bank loading is done
        while (FMODUnity.RuntimeManager.AnyBankLoading())
        {
            yield return null;
        }

        // Allow the scene to be activated. This means that any OnActivated() or Start()
        // methods will be guaranteed that all FMOD Studio loading will be completed and
        // there will be no delay in starting events
        async.allowSceneActivation = true;
        Debug.Log("Loaded!");
        // Keep yielding the co-routine until scene loading and activation is done.
        while (!async.isDone)
        {
            yield return null;
        }
        isLoadingScene = false;
    }
    
    public void LoadMainGame()
    {
        //gameManager.loadingDeath = false;

        if(isLoadingScene)
            return;
        //gameManager.gameState = GameManager.GameState.Default;
        
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        isLoadingScene = true;
        //play animation
        transition.SetTrigger("Start");
        //wait
        yield return new WaitForSeconds(transitionTime);
        transition.SetTrigger("Start");
        //unload Startscreen
        SceneManager.UnloadSceneAsync(1);
        //LoadScene
        // Start an asynchronous operation to load the scene
        AsyncOperation async = SceneManager.LoadSceneAsync(2, LoadSceneMode.Additive);

        // Don't lead the scene start until all Studio Banks have finished loading
        async.allowSceneActivation = false;

        // Iterate all the Studio Banks and start them loading in the background
        // including the audio sample data
        foreach(var bank in Banks)
        {
            FMODUnity.RuntimeManager.LoadBank(bank, true);
        }

        // Keep yielding the co-routine until all the Bank loading is done
        while (FMODUnity.RuntimeManager.AnyBankLoading())
        {
            yield return null;
        }

        // Allow the scene to be activated. This means that any OnActivated() or Start()
        // methods will be guaranteed that all FMOD Studio loading will be completed and
        // there will be no delay in starting events
        async.allowSceneActivation = true;
        Debug.Log("Loaded!");
        // Keep yielding the co-routine until scene loading and activation is done.
        while (!async.isDone)
        {
            yield return null;
        }
        isLoadingScene = false;
    }

    public void LoadStartScene()
    {
        Debug.Log("laod");
        if(isLoadingScene)
            return;

        StartCoroutine(LoadStartScreenAgain());
    }
    IEnumerator LoadStartScreenAgain()
    {
        isLoadingScene = true;

        //play animation
        transition.SetTrigger("Start");
        //wait
        yield return new WaitForSeconds(transitionTime);
        transition.SetTrigger("Start");
        //unload Level
        SceneManager.UnloadSceneAsync(2);
        //LoadScene
        // Start an asynchronous operation to load the scene
        AsyncOperation async = SceneManager.LoadSceneAsync(1, LoadSceneMode.Additive);

        // Don't lead the scene start until all Studio Banks have finished loading
        async.allowSceneActivation = false;

        // Iterate all the Studio Banks and start them loading in the background
        // including the audio sample data
        foreach(var bank in Banks)
        {
            FMODUnity.RuntimeManager.LoadBank(bank, true);
        }

        // Keep yielding the co-routine until all the Bank loading is done
        while (FMODUnity.RuntimeManager.AnyBankLoading())
        {
            yield return null;
        }

        // Allow the scene to be activated. This means that any OnActivated() or Start()
        // methods will be guaranteed that all FMOD Studio loading will be completed and
        // there will be no delay in starting events
        async.allowSceneActivation = true;
        Debug.Log("Loaded!");
        // Keep yielding the co-routine until scene loading and activation is done.
        while (!async.isDone)
        {
            yield return null;
        }
        isLoadingScene = false;
    }
}