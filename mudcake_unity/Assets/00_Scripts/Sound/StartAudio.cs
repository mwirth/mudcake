using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartAudio : MonoBehaviour
{
    bool audioResumed = false;
    public void ResumeAudio()
    {
        if (!audioResumed)
        {
            var result = FMODUnity.RuntimeManager.CoreSystem.mixerSuspend();
            Debug.Log("Mixer suspend: " + result);
            result = FMODUnity.RuntimeManager.CoreSystem.mixerResume();
            Debug.Log("Mixer resume: " + result);
            audioResumed = true;
        }
    }
}
