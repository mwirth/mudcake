using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "ObjectToSpawn", menuName = "ScriptableObjects/ObjectToSpawn", order = 2)]
public class SOObjectToSpawn : ScriptableObject
{
    public string myName;
    public GameObject myModel;
    public Sprite myButtonImage;
    public string MyName { get => myName; set => myName = value; }
    public bool isPlant;
}
