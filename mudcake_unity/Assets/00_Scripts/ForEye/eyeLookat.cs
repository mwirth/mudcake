using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class eyeLookat : MonoBehaviour
{
    private Transform myCamera;
    //private Transform cameraone;

    // Start is called before the first frame update
    void Start()
    {
        myCamera = Camera.main.transform;
    }

    // Update is called once per frame
    void Update()
    {
        //transform.LookAt(lookTarget);
        transform.rotation = Quaternion.LookRotation(transform.position - myCamera.position);
    }
}
