using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;
using Pixelplacement.TweenSystem;

public class RotatorScript : MonoBehaviour
{
    private void Start()
    {
        Tween.Rotate(transform, Vector3.back*180, Space.Self, 10, 0, Tween.EaseInOut, Tween.LoopType.PingPong);
    }
}
